<?php

use Faker\Generator as Faker;

$factory->define(App\Colegios::class, function (Faker $faker) {
    return [
        'nombre' =>$faker->unique()->name,
        'direccion' =>$faker->sentence(3),
        'personaContacto' => $faker->name,
        'telefono' => '',
        'correo' => $faker->unique()->safeEmail,
        'web' => ''
    ];
});
