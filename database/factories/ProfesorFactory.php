<?php

use Faker\Generator as Faker;

$factory->define(App\Profesores::class, function (Faker $faker) {
    static $id = 1;
    return [
        'usuarioId' =>$id++,
        'colegioId' =>rand(1,20),
        'telefono' => " ",
        'correo' => $faker->unique()->safeEmail,
        'catedra' =>$faker->text(15),
        'dni' => " "
    ];
});
