<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TablaColegiosSeeder::class);
        $this->call(TablaUsuariosSeeder::class);
        $this->call(TablaProfesoresSeeder::class);
        $this->call(TablaAlumnosSeeder::class);
    }
}
