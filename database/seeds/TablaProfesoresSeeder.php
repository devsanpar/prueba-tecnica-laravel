<?php

use Illuminate\Database\Seeder;

class TablaProfesoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Profesores::class,20)->create();
    }
}
