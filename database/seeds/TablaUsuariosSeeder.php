<?php

use Illuminate\Database\Seeder;

class TablaUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Usuarios::class,40)->create();

        App\Usuarios::create([
            'nombre'=> 'Jose Humberto',
            'apellido' => 'Parisi Campo',
            'perfil' => 'Administrador',
            'usuario' =>'jhparisi',
            'clave' => bcrypt('123456')
        ]);
    }
}
