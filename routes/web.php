<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login', function () {
    return view('main');
});

Route::get('/listaColegios', function () {
    return view('colegios/listaColegios');
});

Route::get('/listaAlumnos', function () {
    return view('alumnos/listaAlumnos');
});

Route::get('/listaProfesores', function () {
    return view('profesores/listaProfesores');
});

Route::get('/listaUsuarios', function () {
    return view('usuarios/listaUsuarios');
});

Route::get('/agregarUsuario', function () {
    $usuarioId = 0;
    $accion = "Crear";
    return view('usuarios/agregarUsuario', compact('usuarioId', 'accion'));
});

Route::get('/agregarUsuario/{usuarioId}', function ($usuarioId) {
    $accion = "Actualizar";
    return view('usuarios/agregarUsuario', compact('usuarioId', 'accion'));
});

Route::get('/agregarProfesor', function () {
    $profesorId = 0;
    $accion = "Crear";
    return view('profesores/agregarProfesor', compact('profesorId', 'accion'));
});

Route::get('/agregarProfesor/{profesorId}', function ($profesorId) {
    $accion = "Actualizar";
    return view('profesores/agregarProfesor', compact('profesorId', 'accion'));
});

Route::get('/agregarAlumno', function () {
    $alumnoId = 0;
    $accion = "Crear";
    return view('alumnos/agregarAlumno', compact('alumnoId', 'accion'));
});
Route::get('/agregarAlumno/{alumnoId}', function ($alumnoId) {
    $accion = "Actualizar";
    return view('alumnos/agregarAlumno', compact('alumnoId', 'accion'));
});

Route::get('/agregarColegio', function () {
    $colegioId = 0;
    $accion = "Crear";
    return view('colegios/agregarColegio', compact('colegioId', 'accion'));
});
Route::get('/agregarColegio/{colegioId}', function ($colegioId) {
    $accion = "Actualizar";
    return view('colegios/agregarColegio', compact('colegioId', 'accion'));
});

Route::get('/detalleColegio/{colegioId}', function ($colegioId) {
    return view('colegios/detalleColegio', compact('colegioId'));
});