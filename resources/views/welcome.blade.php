@extends('template/headerLogin')

@section('content')
    <section id="login" class="content">

        <!-- @if (session('status_ok'))
            <div class="alert alert-success">
                {{ session('status_ok') }}
            </div>
        @endif

        @if (session('status_error'))
            <div class="alert alert-warning">
                {{ session('status_error') }}
            </div>
        @endif -->


        <div class="row">
            <div class="login-box">
                <div class="login-logo">
                    <b>Administrador</b> de Colegios 
                </div>
                <!-- /.login-logo -->
                <div class="login-box-body">

                    <p class="login-box-msg" style="text-transform: uppercase;"><b>Iniciar sesion Administracion</b></p>

                    <form action="login" method="post">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="usuario" name="txtUsuario" id="txtUsuario"
                                value="jhparisi"   required>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="clave" name="txtClave"
                                   id="txtClave" value="123456" required> 
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <!-- /.col -->
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                            </div>
                            <!-- /.col -->
                        </div>

                        <br>                        

                    </form>


                </div>
                
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection


@section('scriptspagina')
    
@endsection


