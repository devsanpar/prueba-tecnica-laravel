@extends('template/header')

@section('content')
<section class="content-header">
    <h1>
    Nombre del Colegio
    </h1>
</section>
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header">
      <div class="col-xs-2">
          <a href="../listaColegios" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
        </div>
        
        
      </div>

      <!-- /.box-header -->
    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Listado de Profesores</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Listado de Alumnos</a></li>
            </ul>
            <div class="tab-content" ng-controller="colegiosController">
              <div class="tab-pane active" id="tab_1">
                <div ng-init="leerProfesoresColegio({{$colegioId}})"></div>
                <div ng-init="leerAlumnosColegio({{$colegioId}})"></div>
                <div class="row">
                    <div class="col-xs-12">                            
                        <div class="box-header">
                            <div class="col-xs-10">
                                
                            </div>
                            <div class="col-xs-2">
                                <a href="../agregarProfesor" class="btn btn-block btn-primary btn-flat"><i
                                            class="fa fa-edit"></i>Crear </a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->

                        <div class="box-body">
                            <table id="example" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Catedra</th>
                                    <th>Correo</th>
                                    <th>Telefono</th>
                                    <th>DNI</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                        <tr ng-repeat="item in profesores">                                    
                                            <td>[[item.nombre]]</td>
                                            <td>[[item.apellido]]</td>
                                            <td>[[item.dni]]</td>
                                            <td>[[item.catedra]]</td>
                                            <td>[[item.correo]]</td>
                                            <td>[[item.telefono]]</td>
                                            <td>
                                            @php $profesorId = "[[item.profesorId]]"; @endphp  
                                            <a style="margin-left: 20px" href="../agregarProfesor/[[item.profesorId]]"><i class="fa fa-pencil"></i></a> &nbsp;&nbsp;
                                            <a style="margin-left: 20px" href="#" ng-click="eliminarProfesor([[item.profesorId]])"><i class="fa fa-trash"></i></a>
                                            </td>                                    
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        


                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                    <!-- /.row -->
                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-header">
                            <div class="col-xs-10">
                                
                            </div>
                            <div class="col-xs-2">
                                <a href="agregarAlumno" class="btn btn-block btn-primary btn-flat"><i
                                            class="fa fa-edit"></i>Crear </a>
                            </div>
                        </div>

                        <!-- /.box-header -->
                        <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->

                        <div class="box-body">
                            <table id="example5" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Curso</th>
                                    <th>Fecha Nac.</th>
                                    <th>DNI</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
      
                                    <tr ng-repeat="item in alumnos">                                    
                                        <td>[[item.nombre]]</td>
                                        <td>[[item.apellido]]</td>
                                        <td>[[item.curso]]</td>
                                        <td>[[item.fechaNacimiento]]</td>
                                        <td>[[item.dni]]</td>
                                        <td>
                                        @php $alumnosId = "[[item.alumnosId]]"; @endphp  
                                        <a style="margin-left: 20px" href="../agregarAlumno/[[item.alumnosId]]"><i class="fa fa-pencil"></i></a> &nbsp;&nbsp;
                                        <a style="margin-left: 20px" href="#" ng-click="eliminarAlumno([[item.alumnosId]])"><i class="fa fa-trash"></i></a>
                                        </td>                                    
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                            <!-- /.box-body -->
                        
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.tab-pane -->
              
            </div>
            <!-- /.tab-content -->
          </div>
      
    <!-- /.box -->

    

  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('scriptspagina')
    <!-- <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script type="text/javascript">
    var msj= $("#mensaje").val();
    if(msj==1){
        $('.bs-example-modal').modal('show');
    }
    else if(msj==2){
        $('.bs-example-modal_error').modal('show');
    }
    else if(msj==3){
        $('.bs-example-modal_duplicado').modal('show');
    }

    $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1', {
        allowedContent: true,
    });
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });

  $(document).ready(function(){
        $("#selectIdioma").change(function(){
            $("#selectCategorias").removeAttr("disabled");
            var id= $("#selectIdioma").val();
            var catPadre= $("#selectCatPadre").val();
            $.get("ajaxCatCalidadIdioma="+id, function(data) {
                var json= data;
                var datos= JSON.parse(json);
                //elimino las opciones actuales
                $('#selectCategorias option').remove();
                //Creo las nuevas opciones segun el idioma
                for(x=0; x<datos.length; x++) {
                    $('#selectCategorias').append($('<option>', {
                        value: datos[x].idCatCalidad,
                        text: datos[x].nombreCategoria
                    }));
                }
            });
            $("#btnGuardar").removeAttr("disabled","false");
        });//fin change

    })
</script> -->

@stop