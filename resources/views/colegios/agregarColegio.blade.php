@extends('template/header')

@section('content')
<section class="content-header">
    <h1>
    Colegio
    </h1>
</section>
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header">
      <div class="col-xs-2">
          @if($accion=="Crear")
          <a href="listaColegios" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
        @else
            <a href="../listaColegios" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
        @endif
        </div>
        <div class="col-xs-10">
            <div style="padding-left:25% !important" class="box-title"><h1>{{$accion}} Colegio</h1></div>
        </div>
        
      </div>

      <!-- /.box-header -->
    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->
        <div class="box-body" ng-controller="colegiosController">
        @if($accion=="Actualizar")
            <div ng-init="leerColegio({{$colegioId}})"></div>
        @endif
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                @if($colegioId==0)
                    <form ng-submit="agregarColegio()" enctype="multipart/form-data">
                @else
                    <form ng-submit="actualizarColegio({{$colegioId}})" enctype="multipart/form-data">
                @endif                    
                    <input type="hidden" ng-model="nuevoColegio.token" ng-init="nuevoColegio.token='{{{ csrf_token() }}}'" />                    
                    <input type="hidden" ng-model="nuevoColegio.hddcolegioId" ng-init="nuevoColegio.hddcolegioId='{{ $colegioId }}'" />
                    <!-- text input -->
                    <div class="form-group col-xs-12">
                        <label>Nombre</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoColegio.txtNombre" ng-init="nuevoColegio.txtNombre=''" type="text" class="form-control" placeholder="Nombre" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Direccion</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoColegio.txtDireccion" ng-init="nuevoColegio.txtDireccion=''" type="text" class="form-control" placeholder="Principe Pio" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Persona de contacto</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoColegio.txtPersona" ng-init="nuevoColegio.txtPersona=''" type="text" class="form-control" placeholder="Jose" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Telefono</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoColegio.txtTelefono" ng-init="nuevoColegio.txtTelefono=''" type="phone" class="form-control" placeholder="123465798" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Correo</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoColegio.txtCorreo" ng-init="nuevoColegio.txtCorreo=''" type="email" class="form-control" placeholder="colegio@colegio.com" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Web</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoColegio.txtWeb" ng-init="nuevoColegio.txtWeb=''" type="text" class="form-control" placeholder="colegio.com" maxlength="100" required>
                        </div>
                    </div>

                    <div class="col-xs-5"></div>
                    <div class="col-xs-2">
                        <button id="btnGuardar" type="submit" class="btn btn-block btn-success btn-flat"><i class="fa fa-floppy-o"></i> Guardar</button>
                    </div>
                    <div class="col-xs-5"></div>
                </form>
            </div>
            <div class="col-xs-2"></div>
        </div>
      
    <!-- /.box -->

    

  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('scriptspagina')
    <!-- <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script type="text/javascript">
    var msj= $("#mensaje").val();
    if(msj==1){
        $('.bs-example-modal').modal('show');
    }
    else if(msj==2){
        $('.bs-example-modal_error').modal('show');
    }
    else if(msj==3){
        $('.bs-example-modal_duplicado').modal('show');
    }

    $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1', {
        allowedContent: true,
    });
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });

  $(document).ready(function(){
        $("#selectIdioma").change(function(){
            $("#selectCategorias").removeAttr("disabled");
            var id= $("#selectIdioma").val();
            var catPadre= $("#selectCatPadre").val();
            $.get("ajaxCatCalidadIdioma="+id, function(data) {
                var json= data;
                var datos= JSON.parse(json);
                //elimino las opciones actuales
                $('#selectCategorias option').remove();
                //Creo las nuevas opciones segun el idioma
                for(x=0; x<datos.length; x++) {
                    $('#selectCategorias').append($('<option>', {
                        value: datos[x].idCatCalidad,
                        text: datos[x].nombreCategoria
                    }));
                }
            });
            $("#btnGuardar").removeAttr("disabled","false");
        });//fin change

    })
</script> -->

@stop