@extends('template/header')

@section('content')
<section class="content-header">
    <h1>
    Usuario
    </h1>
</section>
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header">
      <div class="col-xs-2">
        @if($accion=="Crear")
          <a href="listaUsuarios" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
        @else
            <a href="../listaUsuarios" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
        @endif
        </div>
        <div class="col-xs-10">
            <!--csrf_token() }} -->
            <div style="padding-left:25% !important" class="box-title"><h1>{{$accion}} Usuario</h1></div>
        </div>
        
      </div>

      <!-- /.box-header -->
    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->
        <div class="box-body" ng-controller="usuariosController">
        @if($accion=="Actualizar")
            <div ng-init="leerUsuario({{$usuarioId}})"></div>
        @endif
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
            @if($usuarioId==0)
                <form ng-submit="agregarUsuario()" enctype="multipart/form-data">
            @else
                <form ng-submit="actualizarUsuario({{$usuarioId}})" enctype="multipart/form-data">
            @endif  
                    <input type="hidden" ng-model="nuevoUsuario.token" ng-init="nuevoUsuario.token='{{{ csrf_token() }}}'" />                    
                    <input type="hidden" ng-model="nuevoUsuario.hddUsuarioId" ng-init="nuevoUsuario.hddUsuarioId='{{ $usuarioId }}'" />
                    <!-- text input -->
                    <div class="form-group col-xs-12">
                        <label>Nombre</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoUsuario.txtNombre" type="text" class="form-control" placeholder="Nombre" maxlength="100" required ng-init="nuevoUsuario.txtNombre=''">
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Apellido</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoUsuario.txtApellido" id="txtApellido" type="text" class="form-control" placeholder="Apellido" maxlength="100" required ng-init="nuevoUsuario.txtApellido=''">
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Usuario</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoUsuario.txtUsuario" id="txtUsuario" type="text" class="form-control" placeholder="usuario" maxlength="100" required ng-init="nuevoUsuario.txtUsuario=''">
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Clave</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoUsuario.pssClave" id="pssClave" type="password" class="form-control" placeholder="Clave" maxlength="100" required ng-init="">
                        </div>
                    </div>

                    <div class="col-xs-5"></div>
                    <div class="col-xs-2">                    
                        <button type="submit" class="btn btn-block btn-success btn-flat"><i class="fa fa-floppy-o"></i> Guardar</button>                   
                    </div>
                    <div class="col-xs-5"></div>
                    
                </form>
            </div>
        </div>
      
    <!-- /.box -->

    

  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('scriptspagina')
    
@stop