@extends('template/header')

@section('content')
    <section class="content-header">
        <h1>
            Alumnos
        </h1>
    </section>
    <section class="content" ng-controller="alumnosController">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-10">
                            <div style="text-align:center !important" class="box-title">Listado de Alumnos</div>
                        </div>
                        <div class="col-xs-2">
                            <a href="agregarAlumno" class="btn btn-block btn-primary btn-flat"><i
                                        class="fa fa-edit"></i>Crear </a>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->

                    <div class="box-body">
                        <table id="example" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Curso</th>
                                <th>Fecha Nac.</th>
                                <th>DNI</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in alumnos">                                    
                                    <td>[[item.nombre]]</td>
                                    <td>[[item.apellido]]</td>
                                    <td>[[item.curso]]</td>
                                    <td>[[item.fechaNacimiento]]</td>
                                    <td>[[item.dni]]</td>
                                    <td>
                                    @php $alumnosId = "[[item.alumnosId]]"; @endphp  
                                    <a style="margin-left: 20px" href="agregarAlumno/[[item.alumnosId]]"><i class="fa fa-pencil"></i></a> &nbsp;&nbsp;
                                    <a style="margin-left: 20px" href="#" ng-click="eliminarAlumno([[item.alumnosId]])"><i class="fa fa-trash"></i></a>
                                    </td>                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scriptspagina')
    
@stop