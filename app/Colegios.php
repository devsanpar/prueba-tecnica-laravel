<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colegios extends Model
{
    protected $table = 'colegios';
    protected $fillable = [];

    public function alumnos(){
        return $this->hasMany('App\Alumnos');
    }

    public function profesores(){
        return $this->hasMany('App\Profesores');
    }
}
