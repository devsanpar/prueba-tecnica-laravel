<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Usuarios;

class UsuariosController extends Controller
{
    //funcion para leer todos los usuarios
    public function index()
    {
        try {
            $usuarios = Usuarios::all();
            return $usuarios;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para buscar por id de un usuario
    public function show($usuarioId)
    {
        $respuesta = "";
        try {
            $usuario = Usuarios::where('usuarioId', '=', $usuarioId)->get();
            if ($usuario->count() == 0) {
                $respuesta = json_encode(array('usuarioId' => 0, 'mensaje' => 'No existen datos para esta consulta'));
            } else {
                $respuesta = $usuario;
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para dar de alta a un usuario
    public function store(Request $request)
    {

        $respuesta = "";
        try {
            $usuario = new Usuarios;
            $usuario->nombre = $request->input("txtNombre");
            $usuario->apellido = $request->input("txtApellido");
            $usuario->perfil = "Administrador";
            $usuario->usuario = $request->input("txtUsuario");
            $usuario->clave = $request->input("pssClave");
            if ($usuario->save()) {
                $respuesta = json_encode(array('usuarioId' => $usuario->id, 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('usuarioId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para actualizar los datos de un usuario
    public function update(Request $request)
    {
        //die($request->input('hddUsuarioId'));
        $respuesta = "";
        try {
            $usuario = Usuarios::where('usuarioId', '=', $request->input('hddUsuarioId'))->update([
                'nombre' => $request->input('txtNombre'),
                'apellido' => $request->input('txtApellido'),
                'usuario' => $request->input('txtUsuario'),
                'clave' => $request->input('pssClave'),
            ]);
            if ($usuario) {
                $respuesta = json_encode(array('usuarioId' => $request->input('hddUsuarioId'), 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('usuarioId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para eliminar un usuario
    public function destroy($usuarioId)
    {
        $respuesta = "";
        try {
            $usuario = Usuarios::where('usuarioId', '=', $usuarioId)->delete();
            if ($usuario) {
                $respuesta = $respuesta = json_encode(array('usuarioId' => $usuarioId, 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('usuarioId' => $usuarioId, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );

        }
    }
}
