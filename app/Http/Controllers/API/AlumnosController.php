<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alumnos;
use App\Usuarios;

class AlumnosController extends Controller
{
    //funcion para leer todos los registros
    public function index()
    {
        try {
            $alumnos = Alumnos::join('Usuarios', 'Alumnos.usuarioId', '=', 'Usuarios.usuarioId')->join('Colegios', 'Alumnos.colegioId', '=', 'Colegios.colegioId')->get(['Usuarios.*', 'Colegios.nombre as Colegio', 'Alumnos.curso', 'Alumnos.dni', 'Alumnos.fechaNacimiento', 'Alumnos.alumnosId']);
            return $alumnos;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para buscar por id de un alumno
    public function show($alumnoId)
    {
        $respuesta = "";
        try {
            $alumno = Alumnos::join('Usuarios', 'Alumnos.usuarioId', '=', 'Usuarios.usuarioId')->join('Colegios', 'Alumnos.colegioId', '=', 'Colegios.colegioId')->where('alumnosId', '=', $alumnoId)->get(['Usuarios.*', 'Colegios.nombre as Colegio', 'Alumnos.curso', 'Alumnos.dni', 'Alumnos.fechaNacimiento', 'Alumnos.alumnosId']);
            if ($alumno->count() == 0) {
                $respuesta = json_encode(array('alumnosId' => 0, 'mensaje' => 'No existen datos para esta consulta'));
            } else {
                $respuesta = $alumno;
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para dar de alta a un alumno
    public function store(Request $request)
    {
        $respuesta = "";
        try {
            $usuario = new Usuarios;
            $usuario->nombre = $request->input("txtNombre");
            $usuario->apellido = $request->input("txtApellido");
            $usuario->perfil = "Alumno";
            $usuario->usuario = $request->input("txtNombre") . "." . $request->input("txtApellido");
            $usuario->clave = 'nuevaClave';
            if ($usuario->save()) {
                $alumno = new Alumnos;
                $alumno->usuarioId = $usuario->id;
                $alumno->colegioId = $request->input("selectColegio");
                $alumno->curso = $request->input("txtCurso");
                $alumno->dni = $request->input("txtDni");
                $alumno->fechaNacimiento = $request->input("datefechaNac");
                if ($alumno->save()) {
                    $respuesta = json_encode(array('alumnosId' => $alumno->id, 'mensaje' => 'OK'));
                } else {
                    $respuesta = json_encode(array('alumnosId' => 0, 'mensaje' => 'ERROR'));
                }
            } else {
                $respuesta = json_encode(array('alumnosId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para actualizar los datos de un alumno
    public function update(Request $request)
    {
        //die("Input" . $request->input('hddAlumnoId'));
        $respuesta = "";
        try {
            $alumno = Alumnos::where('alumnosId', '=', $request->input('hddAlumnoId'))->update([
                'colegioId' => $request->input('selectColegio'),
                'curso' => $request->input('txtCurso'),
                'dni' => $request->input('txtDni')
            ]);
            $usuarioId = Alumnos::where('alumnosId', '=', $request->input('hddAlumnoId'))->get();
            $usuarioId = $usuarioId[0]->usuarioId;
            if ($alumno) {
                $usuario = Usuarios::where('usuarioId', '=', $usuarioId)->update([
                    'nombre' => $request->input('txtNombre'),
                    'apellido' => $request->input('txtApellido')
                ]);
                if ($usuario) {
                    $respuesta = json_encode(array('alumnosId' => $request->input('hddAlumnoId'), 'mensaje' => 'OK'));
                } else {
                    $respuesta = json_encode(array('alumnosId' => 0, 'mensaje' => 'ERROR'));
                }
            } else {
                $respuesta = json_encode(array('alumnosId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para eliminar un alumno
    public function destroy($alumnoId)
    {
        $respuesta = "";
        try {
            $alumno = Alumnos::where('alumnosId', '=', $alumnoId)->delete();
            if ($alumno) {
                $respuesta = $respuesta = json_encode(array('alumnosId' => $alumnoId, 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('alumnosId' => $alumnoId, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );

        }
    }

}
