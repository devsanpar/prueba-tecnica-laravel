<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profesores;
use App\Usuarios;

class ProfesoresController extends Controller
{
    //funcion para leer todos los registros
    public function index()
    {
        try {
            $profesores = Profesores::join('Usuarios', 'Profesores.usuarioId', '=', 'Usuarios.usuarioId')->join('Colegios', 'Profesores.colegioId', '=', 'Colegios.colegioId')->get(['Usuarios.*', 'Colegios.nombre as Colegio', 'Profesores.telefono', 'Profesores.correo', 'Profesores.catedra', 'Profesores.dni', 'Profesores.profesorId']);
            return $profesores;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }
    
    //funcion para buscar por id de un profesor
    public function show($profesorId)
    {
        $respuesta = "";
        try {
            $profesor = Profesores::join('Usuarios', 'Profesores.usuarioId', '=', 'Usuarios.usuarioId')->join('Colegios', 'Profesores.colegioId', '=', 'Colegios.colegioId')->where('profesorId', '=', $profesorId)->get(['Usuarios.*', 'Colegios.nombre as Colegio', 'Profesores.telefono', 'Profesores.correo', 'Profesores.catedra', 'Profesores.dni', 'Profesores.profesorId']);
            if ($profesor->count() == 0) {
                $respuesta = json_encode(array('profesorId' => 0, 'mensaje' => 'No existen datos para esta consulta'));
            } else {
                $respuesta = $profesor;
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para dar de alta a un profesor
    public function store(Request $request)
    {
        $respuesta = "";
        try {
            $usuario = new Usuarios;
            $usuario->nombre = $request->input("txtNombre");
            $usuario->apellido = $request->input("txtApellido");
            $usuario->perfil = "Profesor";
            $usuario->usuario = $request->input("txtNombre") . "." . $request->input("txtApellido");
            $usuario->clave = 'nuevaclave';
            if ($usuario->save()) {
                $profesor = new Profesores;
                $profesor->usuarioId = $usuario->id;
                $profesor->colegioId = $request->input("selectColegio");
                $profesor->telefono = $request->input("txtTelefono");
                $profesor->correo = $request->input("txtCorreo");
                $profesor->catedra = $request->input("txtCatedra");
                $profesor->dni = $request->input("txtDni");
                if ($profesor->save()) {
                    $respuesta = json_encode(array('profesorId' => $profesor->id, 'mensaje' => 'OK'));
                } else {
                    $respuesta = json_encode(array('profesorId' => 0, 'mensaje' => 'ERROR'));
                }
            } else {
                $respuesta = json_encode(array('profesorId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para actualizar los datos de un alumno
    public function update(Request $request)
    {

        $respuesta = "";
        try {
            $profesor = Profesores::where('profesorId', '=', $request->input('hddProfesorId'))->update([
                'colegioId' => $request->input('selectColegio'),
                'telefono' => $request->input('txtTelefono'),
                'correo' => $request->input('txtCorreo'),
                'catedra' => $request->input('txtCatedra'),
                'dni' => $request->input('txtDni'),
            ]);
            $usuarioId = Profesores::where('profesorId', '=', $request->input('hddProfesorId'))->get();
            if ($profesor) {
                $usuario = Usuarios::where('usuarioId', '=', $usuarioId[0]->usuarioId)->update([
                    'nombre' => $request->input('txtNombre'),
                    'apellido' => $request->input('txtApellido')
                ]);
                if ($usuario) {
                    $respuesta = json_encode(array('profesorId' => $request->input('hddProfesorId'), 'mensaje' => 'OK'));
                } else {
                    $respuesta = json_encode(array('profesorId' => 0, 'mensaje' => 'ERROR'));
                }
            } else {
                $respuesta = json_encode(array('profesorId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para eliminar un profesor
    public function destroy($profesorId)
    {
        $respuesta = "";
        try {
            $profesor = Profesores::where('profesorId', '=', $profesorId)->delete();
            if ($profesor) {
                $respuesta = $respuesta = json_encode(array('profesorId' => $profesorId, 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('profesorId' => $profesorId, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );

        }
    }
}
