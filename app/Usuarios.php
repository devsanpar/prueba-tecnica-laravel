<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $table = 'usuarios';
    protected $fillable = [];

    public function alumno(){
        return $this->belongsTo('App\Alumnos');
    }

    public function profesor(){
        return $this->belongsTo('App\Profesores');
    }
}
